GENERAL INFORMATIONS

- This project is coded in Java language and based on Spring Boot framework, i used H2 as database.
- It's compiled with JDK 1.8.121.
- The project comes with a maven wrapper and it should run on an embedded Tomcat.

HOW TO RUN THE PROJECT

1. Open a console;
2. Reach the main project folder;
3. Run this command: <b>mvnw spring-boot:run</b>

Once the application will be up and running you'll find the swagger documentation here:

http://localhost:8080/appointment-scheduler/swagger-ui.html

and the H2 console here (just click "Connect", there is no password):

http://localhost:8080/appointment-scheduler/h2-console


TECHNICAL INFORMATIONS

- Under <b>/src/main/resources</b> there are <b>data.sql</b> and <b>schema.sql</b> that contains DML and DDL.
- You can find all the available attendees in the data.sql file.
