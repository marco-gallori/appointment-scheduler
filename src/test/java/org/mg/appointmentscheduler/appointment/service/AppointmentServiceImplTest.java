/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mg.appointmentscheduler.appointment.persistence.AppointmentDao;
import org.mg.appointmentscheduler.appointment.service.bean.AppointmentInsertDto;
import org.mg.appointmentscheduler.appointment.service.bean.MeetingCreationParams;
import org.mg.appointmentscheduler.appointment.service.bean.ReminderCreationParams;
import org.mg.appointmentscheduler.configuration.AppointmentServiceImplTestConfiguration;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author Marco
 */
@SpringBootTest
@ContextConfiguration(classes =
{
    AppointmentServiceImplTestConfiguration.class
})
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AppointmentServiceImplTest {
    
    @Autowired
    private AppointmentService appointmentService;
    
    @Autowired
    private AppointmentDao appointmentDao;
    
    public static final String REMINDER_TEST_TITLE = "REMINDER_TEST_TITLE";
    public static final String MEETING_TEST_TITLE = "MEETING_TEST_TITLE";
    
    @Test
    public void verifyDaoInteractionsOnReminderCreationMethod() {
        ReminderCreationParams reminderCreationParams = new ReminderCreationParams(REMINDER_TEST_TITLE, LocalDateTime.now());
        appointmentService.createReminder(reminderCreationParams);
        
        InOrder inOrder = Mockito.inOrder(appointmentDao);
        
        inOrder.verify(appointmentDao, Mockito.times(1)).getAppointmentType(Mockito.any());
        inOrder.verify(appointmentDao, Mockito.times(1)).insertAppointment(Mockito.any());
        inOrder.verify(appointmentDao, Mockito.times(1)).get(Mockito.any());
    }
    
    @Test
    public void verifyDaoInteractionsOnMeetingCreationMethod() {
        
        List<Long> attendees = Stream.of(1L, 2L, 3L).collect(Collectors.toList());
        
        MeetingCreationParams meetingCreationParams = new MeetingCreationParams(MEETING_TEST_TITLE, LocalDateTime.now(), attendees);
        appointmentService.createMeeting(meetingCreationParams);
        
        InOrder inOrder = Mockito.inOrder(appointmentDao);
        
        inOrder.verify(appointmentDao, Mockito.times(1)).getAppointmentType(Mockito.any());
        inOrder.verify(appointmentDao, Mockito.times(1)).insertAppointment(Mockito.any());
        inOrder.verify(appointmentDao, Mockito.times(3)).insertAppointmentAttendee(Mockito.any());
        inOrder.verify(appointmentDao, Mockito.times(1)).get(Mockito.any());
    }
    
    @Test
    public void verifyAppointmentMapper() {
        List<Long> attendees = Stream.of(1L, 2L, 3L).collect(Collectors.toList());
        LocalDateTime now = LocalDateTime.now();
        
        MeetingCreationParams meetingCreationParams = new MeetingCreationParams(MEETING_TEST_TITLE, now, attendees);
        AppointmentInsertDto appointmentInsertDto = AppointmentMapper.INSTANCE.toAppointmentInsertDto(meetingCreationParams);
        
        Assert.assertNotNull(appointmentInsertDto);
        Assert.assertEquals(now, appointmentInsertDto.getDate());
        Assert.assertEquals(MEETING_TEST_TITLE, appointmentInsertDto.getTitle());
    }
}
