/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.ws.rest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mg.appointmentscheduler.appointment.service.AppointmentService;
import org.mg.appointmentscheduler.configuration.AppointmentControllerIntegrationlTestConfiguration;
import org.mg.appointmentscheduler.model.Appointment;
import org.mg.appointmentscheduler.model.AppointmentType;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Marco
 */
@SpringBootTest
@EnableWebMvc
@ContextConfiguration(classes =
{
    AppointmentControllerIntegrationlTestConfiguration.class
})
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AppointmentControllerIntegrationTest {
    
    @Autowired 
    private AppointmentService appointmentService;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    private MockMvc mockMvc;
    
    private static final String REMINDER_INSERT_POST_PAH = "/appointments/reminders";
    
    private static final String REMINDER_TITLE = "Dentista";
    
    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }
    
    @Test
    public void verify_createReminderPost_created() throws Exception {
        Appointment appointment = new Appointment();
        appointment.setAppointmentType(AppointmentType.REMINDERS);
        appointment.setAttendees(new ArrayList<>());
        appointment.setId(1L);
        appointment.setTitle(REMINDER_TITLE);
        appointment.setDate(LocalDateTime.of(2022, 4, 3, 12, 0, 0));
        
        Mockito.when(appointmentService.createReminder(Mockito.any())).thenReturn(appointment);
        
        final String REMINDER_INSERT_POST_BODY = "{\"date\": \"2022-04-03T12:00\", \"title\": \"Dentista\"}";
        
        this.mockMvc.perform(MockMvcRequestBuilders.post(REMINDER_INSERT_POST_PAH)
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(REMINDER_INSERT_POST_BODY))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Dentista"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value("2022-04-03T12:00:00"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1L));
    }
    
    @Test
    public void verify_createReminderPost_badRequest() throws Exception {
        Appointment appointment = new Appointment();
        appointment.setAppointmentType(AppointmentType.REMINDERS);
        appointment.setAttendees(new ArrayList<>());
        appointment.setId(1L);
        appointment.setTitle(REMINDER_TITLE);
        appointment.setDate(LocalDateTime.of(2022, 4, 3, 12, 0, 0));
        
        Mockito.when(appointmentService.createReminder(Mockito.any())).thenReturn(appointment);
        
        final String REMINDER_INSERT_POST_BODY = "{\"title\": \"Dentista\"}";
        
        this.mockMvc.perform(MockMvcRequestBuilders.post(REMINDER_INSERT_POST_PAH)
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(REMINDER_INSERT_POST_BODY))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
