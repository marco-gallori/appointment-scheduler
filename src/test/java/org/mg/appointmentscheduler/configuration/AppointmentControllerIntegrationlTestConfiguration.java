/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.configuration;

import org.mg.appointmentscheduler.appointment.service.AppointmentService;
import org.mg.appointmentscheduler.appointment.ws.rest.AppointmentController;
import org.mg.appointmentscheduler.user.service.UserService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;

/**
 *
 * @author Marco
 */
public class AppointmentControllerIntegrationlTestConfiguration {
    @Bean
    public AppointmentService appointmentService() {
        return Mockito.mock(AppointmentService.class);
    }
    
    @Bean
    public UserService userService() {
        return Mockito.mock(UserService.class);
    }
    
    @Bean
    public AppointmentController appointmentController()
    {
        return new AppointmentController();
    }
}
