/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.configuration;

import org.mg.appointmentscheduler.appointment.persistence.AppointmentDao;
import org.mg.appointmentscheduler.appointment.service.AppointmentService;
import org.mg.appointmentscheduler.appointment.service.AppointmentServiceImpl;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;

/**
 *
 * @author Marco
 */
public class AppointmentServiceImplTestConfiguration {
    
    @Bean
    public AppointmentService appointmentService() {
        return new AppointmentServiceImpl();
    }
    
    @Bean
    public AppointmentDao appointmentDao() {
        return Mockito.mock(AppointmentDao.class);
    }
    
}
