insert into appointment_type (id,code) values (1,'MEETINGS');
insert into appointment_type (id,code) values (2,'REMINDERS');

insert into users (id,email,name,surname) values (1, 'lucia.verdi@email.com', 'Lucia', 'Verdi');
insert into users (id,email,name,surname) values (2, 'mario.rossi@email.com', 'Mario', 'Rossi');
insert into users (id,email,name,surname) values (3, 'luca.bianchi@email.com', 'Luca', 'Bianchi');
insert into users (id,email,name,surname) values (4, 'giovanni.messina@email.com', 'Giovanni', 'Messina');
insert into users (id,email,name,surname) values (5, 'martina.merola@email.com', 'Martina', 'Merola');
insert into users (id,email,name,surname) values (6, 'claudia.domenici@email.com', 'Claudia', 'Domenici');