create table appointment_type
(
    id number primary key,
    code varchar2(50)
);
create unique index idx_app_type_code on appointment_type(code);

create sequence seq_user_id start with 1 increment by 1 nocache nocycle order;
create table users
(
    id number primary key,
    email varchar2(100) not null,
    name varchar2(50),
    surname varchar2(50)
);
create unique index idx_users_email on users(email);

create sequence seq_appointment_id start with 1 increment by 1 nocache nocycle order;
create table appointment
(
    id number primary key,
    id_appointment_type number not null,
    title varchar2(100) not null,
    appointment_date timestamp not null
);
alter table appointment add constraint fk_app_id_app_type foreign key (id_appointment_type) references appointment_type(id);

create table appointment_attendee
(
    id_appointment number not null,
    id_user number not null,
    primary key(id_appointment,id_user)
);
alter table appointment_attendee add constraint fk_app_attendee_id_app foreign key (id_appointment) references appointment(id);
alter table appointment_attendee add constraint fk_app_attendee_id_user foreign key (id_user) references users(id);