/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.ws.rest;

import io.swagger.annotations.ApiOperation;
import java.time.LocalDate;
import java.util.List;
import javax.validation.Valid;
import org.mg.appointmentscheduler.appointment.service.AppointmentService;
import org.mg.appointmentscheduler.appointment.service.bean.FindAppointmentDto;
import org.mg.appointmentscheduler.appointment.service.bean.MeetingCreationParams;
import org.mg.appointmentscheduler.appointment.service.bean.ReminderCreationParams;
import org.mg.appointmentscheduler.appointment.ws.rest.mapper.AppointmentResponseMapper;
import org.mg.appointmentscheduler.appointment.ws.rest.request.MeetingInsertRequest;
import org.mg.appointmentscheduler.appointment.ws.rest.request.ReminderInsertRequest;
import org.mg.appointmentscheduler.appointment.ws.rest.request.UpdateAppointmentAttendeeRequest;
import org.mg.appointmentscheduler.appointment.ws.rest.request.UpdateAppointmentDateRequest;
import org.mg.appointmentscheduler.appointment.ws.rest.response.AppointmentResponse;
import org.mg.appointmentscheduler.model.Appointment;
import org.mg.appointmentscheduler.model.AppointmentType;
import org.mg.appointmentscheduler.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Marco
 */
@RestController
public class AppointmentController {
    @Autowired
    private AppointmentService appointmentService;
    
    @Autowired
    private UserService userService;
    
    @ApiOperation(value = "Create a reminder")
    @RequestMapping(path = "/appointments/reminders", method = RequestMethod.POST)
    public ResponseEntity<AppointmentResponse> createReminder(@RequestBody @Valid ReminderInsertRequest reminderInsertRequest)
    {
        ReminderCreationParams reminderCreationParams = new ReminderCreationParams(reminderInsertRequest.getTitle(), reminderInsertRequest.getDate());
        Appointment appointment = appointmentService.createReminder(reminderCreationParams);
        return ResponseEntity.status(HttpStatus.CREATED).body(AppointmentResponseMapper.INSTANCE.toAppointmentResponse(appointment));
    }
    
    @ApiOperation(value = "Create a meeting")
    @RequestMapping(path = "/appointments/meetings", method = RequestMethod.POST)
    public ResponseEntity<AppointmentResponse> createMeeting(@RequestBody @Valid MeetingInsertRequest meetingInsertRequest)
    {
        boolean allAttendeesExist = meetingInsertRequest.getAttendees().stream().allMatch(attendee -> userService.get(attendee) != null);
        
        if (allAttendeesExist) {
            MeetingCreationParams meetingCreationParams = new MeetingCreationParams(meetingInsertRequest.getTitle(), meetingInsertRequest.getDate(), meetingInsertRequest.getAttendees());
            Appointment appointment = appointmentService.createMeeting(meetingCreationParams);
            return ResponseEntity.status(HttpStatus.CREATED).body(AppointmentResponseMapper.INSTANCE.toAppointmentResponse(appointment));
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
    
    @ApiOperation(value = "Update appointment's date")
    @RequestMapping(path = "/appointments/{appointmentId}", method = RequestMethod.PUT)
    public ResponseEntity<AppointmentResponse> updateAppointmentDate(@PathVariable(name = "appointmentId") Long appointmentId,
            @RequestBody @Valid UpdateAppointmentDateRequest updateAppointmentDateRequest) {
        Appointment appointment = appointmentService.get(appointmentId);
        
        if (appointment != null) {
            Appointment appointmentResult = appointmentService.updateAppointmentDate(appointmentId, updateAppointmentDateRequest.getAppointmentDate());
            return ResponseEntity.ok(AppointmentResponseMapper.INSTANCE.toAppointmentResponse(appointmentResult));
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
    
    @ApiOperation(value = "Update appointment's attendees")
    @RequestMapping(path = "/appointments/{appointmentId}/attendees", method = RequestMethod.PUT)
    public ResponseEntity<AppointmentResponse> updateAppointmentAttendees(@PathVariable(name = "appointmentId") Long appointmentId,
            @RequestBody @Valid UpdateAppointmentAttendeeRequest updateAppointmentAttendeeRequest) {
        Appointment appointment = appointmentService.get(appointmentId);
        boolean allAttendeesExist = updateAppointmentAttendeeRequest.getAttendees().stream().allMatch(attendee -> userService.get(attendee) != null);
        
        if (appointment != null && allAttendeesExist && appointment.getAppointmentType().equals(AppointmentType.MEETINGS)) {
            Appointment appointmentResult = appointmentService.updateMeetingAttendees(appointmentId, updateAppointmentAttendeeRequest.getAttendees());
            return ResponseEntity.ok(AppointmentResponseMapper.INSTANCE.toAppointmentResponse(appointmentResult));
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
    
    @ApiOperation(value = "Search an appointment by date")
    @RequestMapping(path = "/appointments/{appointmentDay}", method = RequestMethod.GET)
    public ResponseEntity<List<AppointmentResponse>> find(@PathVariable(name = "appointmentDay") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate appointmentDay,
            @RequestParam(name = "attendeeId", required = false) Long attendeeId) {
        FindAppointmentDto findAppointmentDto = new FindAppointmentDto(appointmentDay);
        findAppointmentDto.setAttendeeId(attendeeId);
        
        List<Appointment> appointments = appointmentService.findAppointments(findAppointmentDto);
        
        return ResponseEntity.ok().body(AppointmentResponseMapper.INSTANCE.toAppointmentResponseList(appointments));
    }
            
}