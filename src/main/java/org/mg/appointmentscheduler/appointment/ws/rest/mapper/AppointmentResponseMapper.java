/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.ws.rest.mapper;

import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.mg.appointmentscheduler.appointment.ws.rest.response.AppointmentResponse;
import org.mg.appointmentscheduler.appointment.ws.rest.response.UserResponse;
import org.mg.appointmentscheduler.model.Appointment;
import org.mg.appointmentscheduler.model.User;

/**
 *
 * @author Marco
 */
@Mapper
public interface AppointmentResponseMapper {
    
    public static final AppointmentResponseMapper INSTANCE = Mappers.getMapper(AppointmentResponseMapper.class);
    
    public List<AppointmentResponse> toAppointmentResponseList(List<Appointment> appointments);
    public AppointmentResponse toAppointmentResponse(Appointment appointment);
    public UserResponse toUserResponse(User user);
}
