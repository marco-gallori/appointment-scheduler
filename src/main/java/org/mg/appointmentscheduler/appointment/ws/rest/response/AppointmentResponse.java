/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.ws.rest.response;

import java.util.List;

/**
 *
 * @author Marco
 */
public class AppointmentResponse {
    
    private Long id;
    private String date;
    private String title;
    private String appointmentType;
    
    private List<UserResponse> attendees;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public List<UserResponse> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<UserResponse> attendees) {
        this.attendees = attendees;
    }
}
