/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.service.bean;

import java.time.LocalDateTime;

/**
 *
 * @author Marco
 */
public class UpdateAppointmentDateDto {
    
    private final Long appointmentId;
    private final LocalDateTime date;

    public UpdateAppointmentDateDto(Long appointmentId, LocalDateTime date) {
        this.appointmentId = appointmentId;
        this.date = date;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }

    public LocalDateTime getDate() {
        return date;
    }
    
}
