/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.service;

import java.time.LocalDateTime;
import java.util.List;
import org.mg.appointmentscheduler.appointment.service.bean.FindAppointmentDto;
import org.mg.appointmentscheduler.appointment.service.bean.MeetingCreationParams;
import org.mg.appointmentscheduler.appointment.service.bean.ReminderCreationParams;
import org.mg.appointmentscheduler.model.Appointment;

/**
 *
 * @author Marco
 */
public interface AppointmentService {
    
    public Appointment get(Long appointmentId);
    
    public Appointment createReminder(ReminderCreationParams reminderCreationParams);
    public Appointment createMeeting(MeetingCreationParams meetingCreationParams);
    
    public Appointment updateAppointmentDate(Long appointmentId, LocalDateTime date);
    public Appointment updateMeetingAttendees(Long appointmentId, List<Long> attendees);
    
    public List<Appointment> findAppointments(FindAppointmentDto findAppointmentDto);
}
