/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.service.bean;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 *
 * @author Marco
 */
public class FindAppointmentDto {
    private final LocalDate appointmentDay;
    
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Long attendeeId;
    
    public FindAppointmentDto(LocalDate appointmentDay) {
        this.appointmentDay = appointmentDay;
    }

    public LocalDate getAppointmentDay() {
        return appointmentDay;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Long getAttendeeId() {
        return attendeeId;
    }

    public void setAttendeeId(Long attendeeId) {
        this.attendeeId = attendeeId;
    }
}
