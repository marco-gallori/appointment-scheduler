/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.service.bean;

import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author Marco
 */
public class MeetingCreationParams {

    private final String title;
    private final LocalDateTime date;
    private final List<Long> attendees;

    public MeetingCreationParams(String title, LocalDateTime date, List<Long> attendees) {
        this.title = title;
        this.date = date;
        this.attendees = attendees;
    }

    public String getTitle() {
        return title;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public List<Long> getAttendees() {
        return attendees;
    }

    
}
