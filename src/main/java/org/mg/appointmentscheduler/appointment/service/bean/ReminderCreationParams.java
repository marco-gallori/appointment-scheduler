/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.service.bean;

import java.time.LocalDateTime;

/**
 *
 * @author Marco
 */
public class ReminderCreationParams {
    
    private final String title;
    private final LocalDateTime date;

    public ReminderCreationParams(String title, LocalDateTime date) {
        this.title = title;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public LocalDateTime getDate() {
        return date;
    }
    
    
}
