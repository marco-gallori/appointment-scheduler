/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.service;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.mg.appointmentscheduler.appointment.service.bean.AppointmentInsertDto;
import org.mg.appointmentscheduler.appointment.service.bean.MeetingCreationParams;
import org.mg.appointmentscheduler.appointment.service.bean.ReminderCreationParams;

/**
 *
 * @author Marco
 */
@Mapper
public interface AppointmentMapper {
    
    public static final AppointmentMapper INSTANCE = Mappers.getMapper(AppointmentMapper.class);
    
    public AppointmentInsertDto toAppointmentInsertDto(ReminderCreationParams reminderCreationParams); 
    public AppointmentInsertDto toAppointmentInsertDto(MeetingCreationParams meetingCreationParams);
}
