/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.service.bean;

/**
 *
 * @author Marco
 */
public class AppointmentAttendeeInsertDto {
    
    private final Long userId;
    private final Long appointmentId;

    public AppointmentAttendeeInsertDto(Long userId, Long appointmentId) {
        this.userId = userId;
        this.appointmentId = appointmentId;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }
    
    
}
