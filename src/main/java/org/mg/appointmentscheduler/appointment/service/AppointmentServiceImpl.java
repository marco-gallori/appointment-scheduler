/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import org.mg.appointmentscheduler.appointment.persistence.AppointmentDao;
import org.mg.appointmentscheduler.appointment.service.bean.AppointmentAttendeeInsertDto;
import org.mg.appointmentscheduler.appointment.service.bean.FindAppointmentDto;
import org.mg.appointmentscheduler.appointment.service.bean.AppointmentInsertDto;
import org.mg.appointmentscheduler.appointment.service.bean.MeetingCreationParams;
import org.mg.appointmentscheduler.appointment.service.bean.ReminderCreationParams;
import org.mg.appointmentscheduler.appointment.service.bean.UpdateAppointmentDateDto;
import org.mg.appointmentscheduler.model.Appointment;
import org.mg.appointmentscheduler.model.AppointmentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marco
 */
@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private AppointmentDao appointmentDao;
    
    @Override
    @Transactional
    public Appointment createReminder(ReminderCreationParams reminderCreationParams) {
        AppointmentInsertDto appointmentInsertDto = AppointmentMapper.INSTANCE.toAppointmentInsertDto(reminderCreationParams);
        appointmentInsertDto.setAppointmentTypeId(appointmentDao.getAppointmentType(AppointmentType.REMINDERS.name()));
        
        appointmentDao.insertAppointment(appointmentInsertDto);
        
        return get(appointmentInsertDto.getId());
    }

    @Override
    @Transactional
    public Appointment get(Long appointmentId) {
        return appointmentDao.get(appointmentId);
    }

    @Override
    @Transactional
    public Appointment createMeeting(MeetingCreationParams meetingCreationParams) {
        AppointmentInsertDto appointmentInsertDto = AppointmentMapper.INSTANCE.toAppointmentInsertDto(meetingCreationParams);
        appointmentInsertDto.setAppointmentTypeId(appointmentDao.getAppointmentType(AppointmentType.MEETINGS.name()));
        
        appointmentDao.insertAppointment(appointmentInsertDto);
        
        meetingCreationParams.getAttendees().stream()
                .forEach(attendee -> appointmentDao.insertAppointmentAttendee(new AppointmentAttendeeInsertDto(attendee, appointmentInsertDto.getId())));
        
        return get(appointmentInsertDto.getId());
    }

    @Override
    @Transactional
    public Appointment updateAppointmentDate(Long appointmentId, LocalDateTime date) {
        UpdateAppointmentDateDto updateAppointmentDateDto = new UpdateAppointmentDateDto(appointmentId, date);
        appointmentDao.updateAppointmentDate(updateAppointmentDateDto);
        return get(appointmentId);
    }

    @Override
    @Transactional
    public Appointment updateMeetingAttendees(Long appointmentId, List<Long> attendees) {
        appointmentDao.deleteAppointmentAttendees(appointmentId);
        attendees.stream()
                .forEach(attendee -> appointmentDao.insertAppointmentAttendee(new AppointmentAttendeeInsertDto(attendee, appointmentId)));
        return get(appointmentId);
    }

    @Override
    @Transactional
    public List<Appointment> findAppointments(FindAppointmentDto findAppointmentDto) {
        findAppointmentDto.setStartDate(findAppointmentDto.getAppointmentDay().atStartOfDay());
        findAppointmentDto.setEndDate(findAppointmentDto.getAppointmentDay().atTime(LocalTime.MAX));
        
        return appointmentDao.findAppointments(findAppointmentDto);
    }
    
}
