/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.persistence;

/**
 *
 * @author Marco
 */
public class AppointmentMapperQueries
{
    public static final String APPOINTMENT_NAMESPACE = "org.mg.appointmentscheduler.appointments.mappers.AppointmentMapper.";
    public static final String GET_APPOINTMENT_TYPE = "getAppointmentType";
    public static final String INSERT_APPOINTMENT = "insertAppointment";
    public static final String GET_APPOINTMENT = "get";
    public static final String INSERT_APPOINTMENT_ATTENDEE = "insertAppointmentAttendee";
    public static final String UPDATE_APPOINTMENT_DATE = "updateAppointmentDate";
    public static final String DELETE_APPOINTMENT_ATTENDEES = "deleteAppointmentAttendees";
    public static final String FIND_APPOINTMENT = "find";
}
