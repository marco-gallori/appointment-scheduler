/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.persistence;

import java.util.List;
import org.mg.appointmentscheduler.appointment.service.bean.AppointmentAttendeeInsertDto;
import org.mg.appointmentscheduler.appointment.service.bean.FindAppointmentDto;
import org.mg.appointmentscheduler.appointment.service.bean.AppointmentInsertDto;
import org.mg.appointmentscheduler.appointment.service.bean.UpdateAppointmentDateDto;
import org.mg.appointmentscheduler.model.Appointment;

/**
 *
 * @author Marco
 */
public interface AppointmentDao {
    
    public Long getAppointmentType(String code);
    public Appointment get(Long appointmentId);
    
    public AppointmentInsertDto insertAppointment(AppointmentInsertDto appointmentInsertDto);
    public AppointmentAttendeeInsertDto insertAppointmentAttendee(AppointmentAttendeeInsertDto appointmentAttendeeInsertDto);
    
    public int updateAppointmentDate(UpdateAppointmentDateDto updateAppointmentDateDto);
    
    public void deleteAppointmentAttendees(Long appointmentId);
    
    public List<Appointment> findAppointments(FindAppointmentDto findAppointmentDto);
}
