/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.appointment.persistence;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import static org.mg.appointmentscheduler.appointment.persistence.AppointmentMapperQueries.*;
import org.mg.appointmentscheduler.appointment.service.bean.AppointmentAttendeeInsertDto;
import org.mg.appointmentscheduler.appointment.service.bean.FindAppointmentDto;
import org.mg.appointmentscheduler.appointment.service.bean.AppointmentInsertDto;
import org.mg.appointmentscheduler.appointment.service.bean.UpdateAppointmentDateDto;
import org.mg.appointmentscheduler.model.Appointment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Marco
 */
@Repository
public class AppointmentDaoImpl implements AppointmentDao {

    @Autowired
    private SqlSession sqlSession;
    
    @Override
    public Long getAppointmentType(String code) {
        return this.sqlSession.selectOne(APPOINTMENT_NAMESPACE.concat(GET_APPOINTMENT_TYPE), code);
    }

    @Override
    public AppointmentInsertDto insertAppointment(AppointmentInsertDto appointmentInsertDto) {
        this.sqlSession.insert(APPOINTMENT_NAMESPACE.concat(INSERT_APPOINTMENT), appointmentInsertDto);
        return appointmentInsertDto;
    }

    @Override
    public Appointment get(Long appointmentId) {
        return this.sqlSession.selectOne(APPOINTMENT_NAMESPACE.concat(GET_APPOINTMENT), appointmentId);
    }

    @Override
    public AppointmentAttendeeInsertDto insertAppointmentAttendee(AppointmentAttendeeInsertDto appointmentAttendeeInsertDto) {
        this.sqlSession.insert(APPOINTMENT_NAMESPACE.concat(INSERT_APPOINTMENT_ATTENDEE), appointmentAttendeeInsertDto);
        return appointmentAttendeeInsertDto;
    }

    @Override
    public int updateAppointmentDate(UpdateAppointmentDateDto updateAppointmentDateDto) {
        return this.sqlSession.update(APPOINTMENT_NAMESPACE.concat(UPDATE_APPOINTMENT_DATE), updateAppointmentDateDto);
    }

    @Override
    public void deleteAppointmentAttendees(Long appointmentId) {
        this.sqlSession.delete(APPOINTMENT_NAMESPACE.concat(DELETE_APPOINTMENT_ATTENDEES), appointmentId);
    }

    @Override
    public List<Appointment> findAppointments(FindAppointmentDto findAppointmentDto) {
        return this.sqlSession.selectList(APPOINTMENT_NAMESPACE.concat(FIND_APPOINTMENT), findAppointmentDto);
    }
}
