package org.mg.appointmentscheduler.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("org.mg.*")
@EnableAutoConfiguration
public class AppointmentschedulerApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(AppointmentschedulerApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AppointmentschedulerApplication.class);
    }
}
