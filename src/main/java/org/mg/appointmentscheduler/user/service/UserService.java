/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.user.service;

import org.mg.appointmentscheduler.model.User;

/**
 *
 * @author Marco
 */
public interface UserService {
    public User get(Long id);
}
