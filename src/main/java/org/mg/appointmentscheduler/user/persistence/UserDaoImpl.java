/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mg.appointmentscheduler.user.persistence;

import org.apache.ibatis.session.SqlSession;
import org.mg.appointmentscheduler.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Marco
 */
@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SqlSession sqlSession;
    
    @Override
    public User get(Long id) {
        return this.sqlSession.selectOne(UserMapperQueries.USERS_NAMESPACE.concat(UserMapperQueries.GET_USER), id);
    }
    
}
